$(document).ready(function (){
    let mainNavigation= document.getElementsByClassName("main-navigation");
    let menutoggle = document.getElementsByClassName("menu-toggle");
    $(document).on('click','.menu-toggle',function(event){
        event.preventDefault();
        // Cuando el menu se cierra
        if(mainNavigation[0].classList.contains("open")){
            mainNavigation[0].classList.remove("open");
            $(".main-navigation").css("display",'none');
            menutoggle[0].innerHTML =
            '<img src="./images/icon-hamburger.svg" alt="close menu button" />';
        } //Cuando abrimos el menu
        else {
            mainNavigation[0].className += " open";
            $(".main-navigation").css("display",'grid');
            menutoggle[0].innerHTML =
            '<img src="./images/icon-close.svg" alt="close menu button" />';
        }
    });
})